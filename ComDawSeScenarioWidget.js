var ComDawSeScenarioWidget = function(box,optionClass) {
    ComDawSeScenarioWidget.baseConstructor.call(this, box, optionClass);
};
mynfc.extending(ComDawSeScenarioWidget, Thing);
ComDawSeScenarioWidget.prototype.onClick = function() {};
ComDawSeScenarioWidget.prototype.onPaint = function() {};
ComDawSeScenarioWidget.prototype.init = function() {
    var scene = getCurrentScene();

    this.par = this.spec.screen['container'];
    var list_x = this.getX();
    var list_y = this.getY() + this.par.getHeight()/2 - this.getHeight()/2;

    var nb = this.getWidth();
    var nbItems;
    if(nb*this.spec.items.length >= this.par.getWidth()){
        nbItems = this.numberItem(this.par.getWidth(), nb);
    }
    else{
        nbItems = nb*this.spec.items.length;
    }

    if (scene.getWidth()<scene.getHeight())
    {
        nbItems -= this.getWidth();
        list_x += (this.par.getWidth()-nbItems)/2;
    }else{
        list_x += (this.par.getWidth()-nbItems)/2;
    }

    var list_w = nbItems+1;
    var list_h = this.getHeight();

    var list_elt_h = list_h;
    var list_elt_w = this.getWidth();


    var list = new List(list_x,list_y,list_w,list_h,list_elt_h,list_elt_w);
    list.setPaintSlider(false);
    list.setListStyle(LIST_STYLE_HORIZONTAL);
    for (var i = 0 ; i < this.spec.items.length ; i++)
    {
        var t = new Thing();
        t.parent = this;
        t.indexItem = i;
        t.onPaint = function (){
            var scene = getCurrentScene();
            scene.setFont(this.parent.spec.font);
            scene.setColor(this.parent.spec.color);
            scene.drawImage(this.parent.spec.url, this.getX(), this.getY());
            var x = this.getX() + this.getWidth()/2 - scene.getTextWidth(this.parent.spec.items[this.indexItem].txt)/2;
            var y = this.getY() + this.getHeight()/2 - scene.getTextHeight(this.parent.spec.items[this.indexItem].txt)/2;
            scene.drawString(this.parent.spec.items[this.indexItem].txt, x, y);
        };
        t.onClick = function(){
            TAGLET_SETTINGS[this.parent.spec.items[this.indexItem].globalVar] = {"value":this.parent.spec.items[this.indexItem].value};
            getCurrentScene().reload();
        };

        list.addThing(t);
    }
    list.addThing(new Thing());
};


ComDawSeScenarioWidget.prototype.numberItem = function(backWidth, itemWidth){
    var width = itemWidth;
    while(width < backWidth){
        width += itemWidth;
    }
    width -= itemWidth;
    return width;
};